var bodyList = [
    [
        {he:"رئيس اللجنة التحضيرية", name:"اسامة العنتري", photo:"person_1",rotate:0},
        {he:"نائب اللجنة التحضيرية", name:"ادريس المخلافي", photo:"person_2",rotate:0},
        {he:"رئيس اللجنة الاعلامية", name:"اسامة الشهابي", photo:"person_3",rotate:0}
    ],
    [
        {he:"رئيس اللجنة الفنية", name:"محمد الوزير", photo:"person_4",rotate:0},
        {he:"رئيس اللجنة المالية", name:"ماجد الخالدي", photo:"person_5",rotate:0},
        {he:"رئيس لجنة العلاقات", name:"عمار محمد", photo:"person_6",rotate:0}
    ],
    [
        {he:"رئيسة اللجنة الثقافية", name:"سمية سلطان", photo:"person_7",rotate:0},
        {he:"رئيسة اللجنة الرقابية", name:"زينب المقطري", photo:"person_8",rotate:0},
        {he:"رئيسة اللجنة الاكاديمية", name:"ندى العبسي", photo:"person_9",rotate:0}
    ],
    [
        {he:"نائبة اللجنة الاعلامية", name:"اسماء الحملي", photo:"person_10",rotate:0},
        {he:"نائبة اللجنة الفنيية", name:"اماني الفتى", photo:"person_11",rotate:0},
        {he:"نائبة اللجنة المالية", name:"امل حسن", photo:"person_12",rotate:0}
    ],     
    [
        {he:"نائب اللجنة الرقابية", name:"مصطفى مفتاح", photo:"person_13",rotate:0},
        {he:"عضو اللجنة الاعلامية", name:"نجلاء الوصابي", photo:"person_14",rotate:0},
        {he:"عضو اللجنة الاعلامية", name:"كوثر العامري", photo:"person_15",rotate:0}
    ], 
    [
        {he:"عضو اللجنة الاعلامية", name:"ايمن الحمادي", photo:"person_16",rotate:0},
        {he:"عضو اللجنة الاعلامية", name:"خولة الحاج", photo:"person_17",rotate:0},
        {he:"عضو اللجنة الثقافية", name:"رضا القاضي", photo:"person_18",rotate:0}
    ],         
    [          
        {he:"عضو اللجنة الثقافية", name:"زينب درويش", photo:"person_19",rotate:0},
        {he:"عضو اللجنة الثقافية", name:"شدى العبسي", photo:"person_20",rotate:0},
        {he:"عضو اللجنة الثقافية", name:"انعم شمسان", photo:"person_21",rotate:0}
    ],
    [
        {he:"عضو اللجنة الفنية", name:"خديجة شمسان", photo:"person_22",rotate:0},
        {he:"عضو اللجنة الفنية", name:"شهاب الورد", photo:"person_23",rotate:0},
        {he:"عضو لجنة العلاقات", name:"ناصر العشاوي", photo:"person_24",rotate:0}
    ], 
    [
        {he:"عضو لجنة العلاقات", name:"غدير السقاف", photo:"person_25",rotate:0},
        {he:"عضو لجنة العلاقات", name:"ريام هويدي", photo:"person_26",rotate:0},
        {he:"عضو لجنة العلاقات", name:"ايثار البصراوي", photo:"person_27",rotate:0}
    ],
    [
        {he:"عضو لجنة العلاقات", name:"غدير الشعثمي", photo:"person_28",rotate:0},
        {he:"عضو اللجنة الرقابية", name:"رحاب العبسي", photo:"person_29",rotate:0},
        {he:"عضو اللجنة الرقابية", name:"عمرو التعزي", photo:"person_30",rotate:0}
    ], 
    [  
        {he:"عضو اللجنة الاكاديمية", name:"فردوس العكاد", photo:"person_31",rotate:0},
    ]
];

var supportList = [
  [
      {name:"فلافل الشام", number:"7112" , photo:"support_1",rotate:0}
  ]
];
    
var head = `
<!doctype html>
<html lang="ar">
  <head>
    <title> الدفعة 43 - إدارة أعمال </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="fonts/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="fonts/fontawesome/css/font-awesome.min.css">

    <!-- Theme Style -->
    <link rel="stylesheet" href="css/style.css">
		<style>
			.hi {
				display: none;
			}
		</style>
  </head>
  <body>

    <header class="site-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-4 site-logo" data-aos="fade"><a href="index.html"><em>الدفعة 43 - إدارة اعمال</em></a></div>
          <div class="col-8">


            <div class="site-menu-toggle js-site-menu-toggle"  data-aos="fade">
              <span></span>
              <span></span>
              <span></span>
            </div>
            <!-- END menu-toggle -->

            <div class="site-navbar js-site-navbar">
              <nav role="navigation">
                <div class="container">
                  <div class="row full-height align-items-center">
                    <div class="col-md-6">
                      <ul class="list-unstyled menu">
                        <li class="active"><a href="index.html">الرئيسية</a></li>
                        <li><a href="about.html">من نحن</a></li>
                        <li><a href="doctors.html">اعضاء هيئة التدريس</a></li>
                        <li><a href="blog.html">المدونة</a></li>
                      </ul>
                    </div>
                    <div class="col-md-6 extra-info">
                      <div class="row">
                        <div class="col-md-6 mb-5">
                          <h3>معلومات التواصل</h3>
                          <p> كلية التجارة <br> جامعة صنعاء</p>
                          <p></p>

                        </div>
                        <div class="col-md-6">
                          <h3>صفحاتنا على مواقع التواصل</h3>
                          <ul class="list-unstyled">
                            <li><a href="https://www.facebook.com/Managers.passion/">Facebook</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </header>
    <!-- END head -->

    <section class="site-hero overlay" style="background-image: url(img/hero_1.jpg)">
      <div class="container">
        <div class="row site-hero-inner justify-content-center align-items-center">
          <div class="col-md-10 text-center">
            <h1 class="heading" data-aos="fade-up"> <em>الدفعة 43 - إدارة أعمال</em> </h1>
            <p class="sub-heading mb-5" data-aos="fade-up" data-aos-delay="100"> شغف إداري </p>
            <p data-aos="fade-up" data-aos-delay="100"></p>
          </div>
        </div>
        <!-- <a href="#" class="scroll-down">Scroll Down</a> -->
      </div>
    </section>
    <!-- END section -->


    <!-- END section -->

    <section class="section slider-section">
      <div class="container">
        <div class="row justify-content-center text-center mb-5">
          <div class="col-md-8">
            <h2 class="heading" data-aos="fade-up">دفعة شغف إداري</h2>
            <p class="lead" data-aos="fade-up" data-aos-delay="100">بالعزيمة بدأنا ... وبالتميز تفردنا <br> وﻷن النجاح يبدا من الصفر، مشيناها خطى ثابتة، متمكنة ذات أثر، لاتمحى بل تُنحت على الحجر .. فلا تتحقق اﻷشياء المهمة في هذا العالم إلا ﻷولئك الذين اصروا على النجاح .. </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="home-slider major-caousel owl-carousel mb-5" data-aos="fade-up" data-aos-delay="200">
              <div class="slider-item">
                <img src="img/slider-1.jpg" alt="Image placeholder" class="img-fluid">
              </div>
              <div class="slider-item">
                <img src="img/slider-2.jpg" alt="Image placeholder" class="img-fluid">
              </div>
              <div class="slider-item">
                <img src="img/slider-3.jpg" alt="Image placeholder" class="img-fluid">
              </div>
              <div class="slider-item">
                <img src="img/slider-4.jpg" alt="Image placeholder" class="img-fluid">
              </div>
              <div class="slider-item">
                <img src="img/slider-5.jpg" alt="Image placeholder" class="img-fluid">
              </div>
              <div class="slider-item">
                <img src="img/slider-6.jpg" alt="Image placeholder" class="img-fluid">
              </div>
            </div>
            <!-- END slider -->
          </div>

          <div class="col-md-12 text-center">دفعتنا لها قلب مؤمن بالفكرة، وعزيمة كافرة بالفشل. <br> والله الموفق نسأله أن يسدد خطانا ويحقق احلامنا وينير دروبنا.</div>

        </div>
      </div>
    </section>
    <!-- END section -->

    <!-- END section -->
`;

var foot = `
    <footer class="section footer-section">
      <div class="container">
        <div class="row mb-4">
          <div class="col-md-3 mb-5">
            <ul class="list-unstyled link">
              <li class="active"><a href="index.html">الرئيسية</a></li>
              <li><a href="about.html">من نحن</a></li>
              <li><a href="doctors.html">أعضاء هيئة التدريس</a></li>
              <li><a href="blog.html">المدونة</a></li>
            </ul>
          </div>
          <div class="col-md-3 mb-5 pr-md-5 contact-info">
            <p><span class="d-block">العنوان:</span> <span> كلية التجارة، جامعة صنعاء</span></p>
            <p><span class="d-block">حسابنا على فيسبوك:</span> <span> <a href="https://www.facebook.com/Managers.passion/">Facebook</a> </span></p>
          </div>
        </div>

      </div>
    </footer>

    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <!-- <script src="js/jquery.waypoints.min.js"></script> -->
    <script src="js/aos.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>

`;

var support = `    <section class="section testimonial-section" dir="rtl">
      <div class="container">
        <div class="row justify-content-center text-center mb-5">
          <div class="col-md-8">
            <h2 class="heading" data-aos="fade-up">الداعمون</h2>
          </div>
        </div>`;
for (row of supportList){
    support += `<div class="row">`
    for (ele of row){
        support += `
          <div class="col-md-4" data-aos="fade-up" data-aos-delay="100">
            <div class="testimonial text-center">
              <div class="author-image mb-3">
                <img style="transform:rotate(${ele.rotate}deg);border-radius:5% !important" src="img/index/${ele.photo}.jpg" alt="Image placeholder" class="rounded-circle">
              </div>
              <p><em><span style="font-weight:bold;"> ${ele.name} </span><br/>  ${ele.number}</em></p>

            </div>
          </div>
        `
    };
    support += `</div>`
};
support += `</div
      </div>
    </section>`;

var body = `<section class="section testimonial-section" dir="rtl">
      <div class="container">
        <div class="row justify-content-center text-center mb-5">
          <div class="col-md-8">
            <h2 class="heading" data-aos="fade-up">أعضاء اللجنة</h2>
          </div>
        </div>`;
for (row of bodyList){
    body += `<div class="row">`;
    for (ele of row){
        body += `
          <div class="col-md-4" data-aos="fade-up" data-aos-delay="100">
            <div class="testimonial text-center">
              <div class="author-image mb-3">
                <img style="transform:rotate(${ele.rotate}deg);" src="img/index/${ele.photo}.jpg" alt="Image placeholder" class="rounded-circle">
              </div>
              <p><em>&mdash; ${ele.he}<br/>  ${ele.name}</em></p>

            </div>
          </div>
        `
    };
    body += `</div>`
};
body += `</div
      </div>
    </section>`;

console.log( head + support + body + foot );